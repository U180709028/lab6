public class Main {
	public static void main(String[] args) {
		Point p = new Point(4,6);
		Rectangle r = new Rectangle(p,5,9);
		System.out.println("Rectangle's area : " + r.area());
		System.out.println("Rectangle's perimeter : " + r.perimeter());
		Point[] points = r.corners();
		for(int i = 0; i < 4; i++) {
			System.out.println("Rectangle's corner: " + (i + 1) + "" + points[i].xCoord + "," + points[i].yCoord);
		}

		Circle c1 = new Circle(10,p);
		System.out.println("Circle's area: " + c1.area());
		System.out.println("Circle's perimeter: " + c1.perimeter());
		Point p2 = new Point(28,8);
		Circle c2 = new Circle(5,p2);
		System.out.println("Circle 1 and Circle 2 intersect : " + c1.intersect(c2));
	}
}