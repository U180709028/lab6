public class Circle {
	int radius;
	Point center;

	public Circle(int r, Point c) {
		this.radius = r;
		this.center = c;
	}
	public double area() {
		return Math.PI * (radius * radius);
	}
	public double perimeter() {
		return 2 * Math.PI * radius;
	}
	public boolean intersect(Circle c2) {
		int sum = this.radius + c2.radius;
		double m = Math.sqrt(Math.pow((this.center.xCoord - c2.center.xCoord),2) + Math.pow((this.center.yCoord - c2.center.yCoord),2));
		if (m <= sum) {
			return true;

		}else{
			return false; 
		}
	}
}